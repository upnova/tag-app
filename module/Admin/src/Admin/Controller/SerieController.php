<?php
/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Seditag APP
 * ///////////////////////////////////////////////////////////////////////////////
 */
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Application\Model\Entity\DinamicView;
use Application\Model\Entity\TagTable;
use Application\Model\Entity\EstadoTagTable;
use Application\Model\Entity\ClaseVehTable;
use Application\Model\Entity\CargaTagTable;

use DOMPDFModule\View\Model\PdfModel;
use DOMPDFModule\View\Renderer\PdfRenderer;

use Zend\Session\Container;

use Admin\Form\SerieForm;


class SerieController extends AbstractActionController
{
	
	private $sid;
    public function __construct() {
    	//Validamos sesion activa para el modulo
        $this->sid = new Container('base');        
        if($this->sid->offsetGet('urlHome') != APP::URL_ADMIN || $this->sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }
    }

    public function indexAction()
    {	
    	
        //Conectamos con BBDD
        $dbAdapter=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Retornamos a la vista
        $result = new ViewModel();
        $result->setTerminal(true);
        return $result;  

    }

    public function cargaAction()
    {

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Cargamos FORM
        $form = new SerieForm("form");

        $form->get('automotora')->setAttribute('options',(new DinamicView(VMAPP::autom,$db))->getComboAutom());
        $form->get('clase')->setAttribute('options',(new ClaseVehTable($db))->getComboClaseVeh());

        $result = new ViewModel(array('form'=>$form));
        $result->setTerminal(true);
        return $result;        
    }

    public function insertarAction()
    {
        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   
        $sid = new Container('base');        

        //Obtenemos datos POST
        $data = $this->request->getPost();
        
        //Identificamos data con usuario        
        $usuario = $sid->offsetGet('usuario');
        $data['user_create'] = $usuario[0]['id'];

        //Validamos si archivo adjunto existe y si es compatible        
        $adapterFile = new \Zend\File\Transfer\Adapter\Http();
        if(null != $adapterFile->getFileName()){
            $file = date('dmY-His_').basename($adapterFile->getFileName());    

            $eof = substr($file,-3);
            if ( $eof == "txt") {
                
                //Guardamos el archivo original
                $directorio = $_SERVER['DOCUMENT_ROOT'].'/series/';                
                $adapterFile->setDestination($directorio);     
                $adapterFile->addFilter('File\Rename', array('target' =>$file,'overwrite' => true));
                $adapterFile->receive();    
                
                //Seteamos Estado a los TAGS
                $estado = (new EstadoTagTable($db))->getEstado(APP::TAG_LIBRE);
                $data['id_estado'] = $estado[0]['id'];

                //Registramos proceso de carga
                $data['id_carga'] = (new CargaTagTable($db))->nuevo($data);

                //Recorremos archivo con series e insertamos
                $TagTable = new TagTable($db);
                $fh = fopen($directorio.$file,'r');
                $c_series = 0;
                while ($line = fgets($fh)) {                    
                     $data['serie'] = trim($line);
                     if (count($TagTable->getTag($data['serie']))<1) {
                         $TagTable->nuevoTag($data);
                         $c_series++;
                     }
                }
                fclose($fh);        


            }else{
                //Retornamos JSON a la vista             
                return new JsonModel(array('status' =>'nok',                                   
                        'desc'=>"Formato del archivo inválido, debe ser .txt")
                );
            }   
        }else{
            return new JsonModel(array('status'=>'nok','desc'=>"Falta el archivo con las series"));
        }
        return new JsonModel(array('status'=>'ok','desc'=>$c_series." series cargadas OK"));
    }

    public function tablaAction()
    {
        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        $result = new ViewModel();
        $result->setTerminal(true);
        return $result;        
    }

    public function procesoAction()
    {   
        
        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Retornamos a la vista
        $result = new ViewModel();
        $result->setTerminal(true);
        return $result;  

    }

    public function procesoGetAction()
    {
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Consultamos y Retornamos Dotacion
        $DView = new DinamicView(VMAPP::carga_tag,$db);
        return new JsonModel($DView->getCarga());
    }

    public function getAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Consultamos y Retornamos Dotacion
        $DView = new DinamicView(VMAPP::tag,$db);
        return new JsonModel($DView->getTag());        
    }

    public function borraAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos datos POST
        $data = $this->request->getPost();  

        //Borramos Tag
        $TagTable = new TagTable($db);
        $TagTable->borrar($data['serie']);

        //Retornamos a la vista
        return new JsonModel(array('status'=>'ok','desc'=>"Serie eliminada"));    
    }

    public function actaPdfAction()
    {
        $pdf = new PdfModel();      
        $id_carga = $this->params()->fromRoute('id', 0);          

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');
        $tag = (new TagTable($db))->getTagCarga($id_carga);        
        $carga = (new DinamicView(VMAPP::carga_tag,$db))->getCargaId($id_carga);                
        //Cargamos PDF Con variables de contrato        
        $pdf->setVariables(array(
            'tag' => $tag,
            'carga'=> $carga,
            ));        
        return $pdf;
    }
    
}
