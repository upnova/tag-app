<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;
use Application\Util\Basic;

use Application\Model\Entity\DinamicView;

use Zend\Session\Container;


class IndexController extends AbstractActionController
{
    public function indexAction()
    {	
    	//Validamos sesion activa para el modulo
        $sid = new Container('base');        
        if($sid->offsetGet('urlHome') != APP::URL_ADMIN || $sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Obtenemos datos de tiles
        $dash = (new DinamicView(VMAPP::dash,$db))->getDash();
        
        //Contratos Por MES
        $cont_mes = (new DinamicView(VMAPP::cont_mes,$db))->getContMes();
        $data  = array();
        for ($i=0; $i < count($cont_mes) ; $i++) { 
           $data[$i] = array(strtoupper(Basic::getMes($cont_mes[$i]['mes'])) =>$cont_mes[$i]['contratos']);
        }
        
        //Obtenemos series totales
        $series = (new DinamicView(VMAPP::aut_serie,$db))->getSeriesTotales();        

        $clave = Encr::getPass("anamaria","seditag");
        $this->layout('layout/admin');
        return new ViewModel(array('clave'=>$clave,'dash'=>$dash[0],'data'=>$data,'series'=>$series));

    }

    public function escritorioAction()
    {

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');           

        //Obtenemos datos de tiles
        $dash = (new DinamicView(VMAPP::dash,$db))->getDash();        
        
        //Contratos Por MES
        $cont_mes = (new DinamicView(VMAPP::cont_mes,$db))->getContMes();
        $data  = array();
        for ($i=0; $i < count($cont_mes) ; $i++) { 
           $data[$i] = array(strtoupper(Basic::getMes($cont_mes[$i]['mes'])) =>$cont_mes[$i]['contratos']);
        }

        //Obtenemos series totales
        $series = (new DinamicView(VMAPP::aut_serie,$db))->getSeriesTotales();  

        $result = new ViewModel(array('dash'=>$dash[0],'data'=>$data,'series'=>$series));
        $result->setTerminal(true);
        return $result;        
    }
    
}
