<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Application\Model\Entity\DinamicView;
use Application\Model\Entity\TagTable;
use Application\Model\Entity\EstadoContratoTable;
use Application\Model\Entity\ContratoTable;
use Application\Model\Entity\FileContratoTable;

use DOMPDFModule\View\Model\PdfModel;

use Zend\Session\Container;


class ContratoController extends AbstractActionController
{
	
	private $sid;
    public function __construct() {
    	//Validamos sesion activa para el modulo
        $this->sid = new Container('base');        
        if($this->sid->offsetGet('urlHome') != APP::URL_ADMIN || $this->sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }
    }

    public function indexAction()
    {	
    	
        //Conectamos con BBDD
        $dbAdapter=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Retornamos a la vista
        $result = new ViewModel();
        $result->setTerminal(true);
        return $result;  

    }

    public function getAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Consultamos y Retornamos Dotacion
        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContAll();
        return new JsonModel($contrato);        
    }
    
    public function formAdjuntarAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos datos POST
        $data = $this->request->getPost();

        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContrato($data['id']);
        //Retornamos a la vista
        $result = new ViewModel(array('contrato'=>$contrato));
        $result->setTerminal(true);
        return $result;     
    }

    public function marcarAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos datos POST
        $data = $this->request->getPost();

        (new ContratoTable($db))->marcaContrato($data['id']);

         //Retornamos JSON a la vista             
            return new JsonModel(array('status' =>'ok',                                   
                        'desc'=>"Contrato marcado con observación"));    
    }

    public function adjuntosAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos datos POST
        $data = $this->request->getPost();   

        $file = (new FileContratoTable($db))->getFiles($data['id']); 
        $files = array();        
        for ($i=0; $i < count($file) ; $i++) { 
            $title = explode("_",$file[$i]['nombre']); 
            $files[$i]['title'] = ucwords(mb_strtolower($title[2]));
            $files[$i]['file'] = APP::URL_FILES.$file[$i]['nombre'];
        }

        //Retornamos a la vista
        $result = new ViewModel(array('files'=>$files));
        $result->setTerminal(true);
        return $result;     
    }

    public function adjuntarAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos datos POST
        $data = $this->request->getPost();        

        //Guardamos PDF
        $adapterFile = new \Zend\File\Transfer\Adapter\Http();        
        $eof = substr(basename($adapterFile->getFileName()),-3);
        if ( $eof == "pdf") {
            
            //Guardamos el archivo original
            $file = $data['id_contrato'].'_anexo.'.$eof;             
            $directorio = $_SERVER['DOCUMENT_ROOT'].'/files_contrato/';
            $adapterFile->setDestination($directorio);     
            $adapterFile->addFilter('File\Rename', array('target' =>$file,'overwrite' => true));
            $adapterFile->receive();    

            //Cambiamos estado al contrato
            $estado = (new EstadoContratoTable($db))->getEstado(APP::CONT_OK);
            (new ContratoTable($db))->actualizaEstado($estado[0]['id'],$data['id_contrato']);

            //Retornamos JSON a la vista             
            return new JsonModel(array('status' =>'ok',                                   
                        'desc'=>"Anexo asociado exitosamente !"));

        }else{
            //Retornamos JSON a la vista             
            return new JsonModel(array('status' =>'nok',                                   
            'desc'=>"Formato del archivo inválido, debe ser .pdf"));
        }      
    }

}
