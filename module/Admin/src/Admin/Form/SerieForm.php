<?php
namespace Admin\Form;

use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Captcha;
use Zend\Form\Factory;

class SerieForm extends Form
{
     
     public function __construct($name = null)
     {
        parent::__construct($name);              
        
       // Select ///////////////////////// AUTOMOTORA
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'automotora',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'automotora',
                'required' => 'true'
             )
        ));

        // Select ///////////////////////// CLASE
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'clase',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'clase',
                'required' => 'true'
             )
        ));

        // Submit ///////////////////////// ENVIAR FORMULARIO
        $this->add(array(
            'name' => 'enviar',
            'attributes' => array(                
                'type' => 'submit',
                'id' => 'send_series',
                'value' => 'Cargar Series',
                'title' => 'Enviar',            
                'class' => 'btn btn-success pull-right',                
            ),
        ));  
     }
}