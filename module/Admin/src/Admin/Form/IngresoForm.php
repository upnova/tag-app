<?php
namespace App\Form;

use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Captcha;
use Zend\Form\Factory;

class CargaSerieForm extends Form
{
     
     public function __construct($name = null)
     {
        parent::__construct($name);        

        $this->add(array('type' => 'hidden','name' => 'actualiza_persona', 'attributes' => array('value'=>'0','id' => 'actualiza_persona')));
        
       // Select ///////////////////////// COMUNA
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'automotora',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'automotora',
             )
        ));

        // Select ///////////////////////// TIPO VEHICULO
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'name' => 'tipo',
                'id' => 'tipo_vehiculo',
                'class' => 'form-control',
                'required' => 'true',
            )
        ));

        // Select ///////////////////////// MARCA
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'marca',
         
            'attributes' => array(
                'class' => ' select2 form-control',
                // 'required' => 'true',	
                'id' => 'marca',                                   
            )
        ));

        // Select ///////////////////////// MODELO
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'modelo',
         
            'attributes' => array(
                'class' => ' select2 form-control',
                // 'required' => 'true',    
                'id' => 'modelo',                                   
            )
        ));

        // tEXT ///////////////////////// PATENTE
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'patente',
                'id' => 'patente',
                'class' => 'form-control',
                'required' => 'true',
                'autocomplete' => 'off',                
            )
        ));

        // Select ///////////////////////// N° CHASIS
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'chasis',
                'id' => 'chasis',
                'class' => 'form-control',                
                'autocomplete' => 'off',
            )
        ));

        // Number ///////////////////////// KILOMETRAJE
        $this->add(array(
            'type' => 'number',
            'attributes' => array(
                'name' => 'kilometraje',
                'id' => 'kilometraje',
                'class' => 'form-control',
                'autocomplete' => 'off',
            )
        ));

        // Select ///////////////////////// COLOR
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'color',
                'id' => 'color',
                'class' => 'form-control',                                
            )
        ));

        // Select ///////////////////////// YEAR - AÑO
        $this->add(array(
            'type' => 'number',
            'attributes' => array(
                'name' => 'year',
                'id' => 'year',
                'class' => 'form-control',
            )
        ));

        // Submit ///////////////////////// ENVIAR FORMULARIO
        $this->add(array(
            'name' => 'enviar',
            'attributes' => array(                
                'type' => 'submit',
                'id' => 'send_ingreso',
                'value' => 'Ingresar Vehículo',
                'title' => 'Enviar',            
                'class' => 'btn btn-success',                
            ),
        ));  
     }
}