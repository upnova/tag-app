<?php
namespace App\Form;

use Zend\Captcha\AdapterInterface as CaptchaAdapter;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Captcha;
use Zend\Form\Factory;

class TagForm extends Form
{
     
     public function __construct($name = null)
     {
        parent::__construct($name);                
        
        // Text ///////////////////////// NOMBRE CLIENTE
        $this->add(array(
            'type' => 'text',                    
            'name' => 'nombre',
            'attributes' => array(
                'id'=>'nombre_cliente',
                'class' => 'form-control',
                'required' => 'true',
                'autocomplete' => 'off',
            )
        ));

         // Text ///////////////////////// APELLIDO CLIENTE
        $this->add(array(
            'type' => 'text',                    
            'name' => 'apellido',
            'attributes' => array(
                'id'=>'apellido_cliente',
                'class' => 'form-control',
                'required' => 'true',
                'autocomplete' => 'off',
            )
        ));

         // Text ///////////////////////// APELLIDO 2 CLIENTE
        $this->add(array(
            'type' => 'text',                    
            'name' => 'apellido2',
            'attributes' => array(
                'id'=>'apellido2_cliente',
                'class' => 'form-control',
                'required' => 'true',
                'autocomplete' => 'off',
            )
        ));
       
        // Text ///////////////////////// RUT CLIENTE              
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'rut',
                'id' => 'rut_cliente',
                'class' => 'form-control',                                              
                'onchange' => 'validaRut("rut_cliente","send_alta")',
                'required' => 'true',
                'autocomplete' => 'off',                
            )
        ));     
       
        // Text ///////////////////////// TELEFONO
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'telefono',
                'id' => 'telefono',
                'class' => 'form-control',
                'autocomplete' => 'off',
                'required' => 'true',
                //'placeholder'=>'Nombre Condominio',
            )
        ));
        
        // Text ///////////////////////// EMAIL
        $this->add(array(
            'type' => 'email',
            'attributes' => array(
                'name' => 'correo',
                'id' => 'correo',
                'autocomplete' => 'off',
                'class' => 'form-control',
            )
        ));

        // Text ///////////////////////// DIRECCION
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'direccion',
                'id' => 'direccion',
                'class' => 'form-control',                           
                'style' => 'max-width: 100%;',
                'autocomplete' => 'off',
            )
        ));

        // Select ///////////////////////// REGION
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'region',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'region',
                'onchange'=>'comboComuna()'
             )
        ));
        
       // Select ///////////////////////// COMUNA
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'comuna',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comuna',
             )
        ));

        // Select ///////////////////////// TIPO VEHICULO
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'name' => 'clase',
                'id' => 'tipo_vehiculo',
                'class' => 'form-control',
                'required' => 'true',
            )
        ));

        // Select ///////////////////////// MARCA
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'marca',
         
            'attributes' => array(
                'class' => ' select2 form-control',
                // 'required' => 'true',	
                'id' => 'marca',                                   
            )
        ));

        // Select ///////////////////////// MODELO
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'modelo',
         
            'attributes' => array(
                'class' => ' select2 form-control',
                // 'required' => 'true',    
                'id' => 'modelo',                                   
            )
        ));

        // tEXT ///////////////////////// PATENTE
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'patente',
                'id' => 'patente',
                'class' => 'form-control',
                'required' => 'true',
                'autocomplete' => 'off',
                'style'=>'text-transform:uppercase'              
            )
        ));

        // Select ///////////////////////// N° CHASIS
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'chasis',
                'id' => 'chasis',
                'class' => 'form-control',                
                'autocomplete' => 'off',
            )
        ));

        // Number ///////////////////////// KILOMETRAJE
        $this->add(array(
            'type' => 'number',
            'attributes' => array(
                'name' => 'kilometraje',
                'id' => 'kilometraje',
                'class' => 'form-control',
                'autocomplete' => 'off',
            )
        ));

        // Select ///////////////////////// COLOR
        $this->add(array(
            'type' => 'text',
            'attributes' => array(
                'name' => 'color',
                'id' => 'color',
                'class' => 'form-control',
                'style'=>'text-transform:uppercase',
            )
        ));

        // Select ///////////////////////// YEAR - AÑO
        $this->add(array(
            'type' => 'number',
            'attributes' => array(
                'name' => 'year',
                'id' => 'year',
                'class' => 'form-control',
                'required' => 'true',
            )
        ));

        // Select ///////////////////////// FECHA
        $this->add(array(
            'type' => 'date',
            'attributes' => array(
                'name' => 'fecha',
                'id' => 'fecha',
                'class' => 'form-control input-sm',
            )
        ));

        // Select ///////////////////////// SERIE
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'serie',
         
            'attributes' => array(
                'class' => ' select2 form-control',
                'required' => 'true',    
                'id' => 'serie',                                   
            )
        ));

        // Text ///////////////////////// SERIE DEVOLUCION
        $this->add(array(
            'type' => 'text',                    
            'name' => 'serie_devolucion',
            'attributes' => array(
                'id'=>'serie_devolucion',
                'class' => 'form-control',
                'required' => 'true',
            )
        ));

        // Submit ///////////////////////// ENVIAR FORMULARIO
        $this->add(array(
            'name' => 'enviar',
            'attributes' => array(                
                'type' => 'submit',
                'id' => 'send_tag',
                'value' => 'Enviar Alta',
                'title' => 'Enviar',            
                'class' => 'btn btn-success',                
            ),
        ));  
     }
}