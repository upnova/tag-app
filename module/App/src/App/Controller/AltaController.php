<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Application\Model\Entity\DinamicView;

use Zend\Session\Container;


class AltaController extends AbstractActionController
{
    public function indexAction()
    {	
    	//Validamos sesion activa para el modulo
        $sid = new Container('base');        
        if($sid->offsetGet('urlHome') != APP::URL_APP || $sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Retornamos a la vista
        $result = new ViewModel();
        $result->setTerminal(true);
        return $result;    

    }

    public function getAction()
    {   
        //Conector con BBDD
        $db = $this->getServiceLocator()->get('Zend/Db/Adapter');

        //Obtenemos id_automotora
        $sid = new Container('base');
        $usuario = $sid->offsetGet('usuario');

        //Consultamos y Retornamos Dotacion
        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContAutom($usuario[0]['id_automotora']);
        return new JsonModel($contrato);        
    }

    
}
