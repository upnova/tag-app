<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Application\Model\Entity\DinamicView;

use Zend\Session\Container;


class IndexController extends AbstractActionController
{
    public function indexAction()
    {	
    	//Validamos sesion activa para el modulo
        $sid = new Container('base');        
        if($sid->offsetGet('urlHome') != APP::URL_APP || $sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }

                //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Obtenemos datos de la sesion        
        $usuario = $sid->offsetGet('usuario');
        $id_autom = $usuario[0]['id_automotora'];

        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContAutom($id_autom);
        $dashboard['fin_ok'] = 0;
        $dashboard['proceso'] = 0;
        for ($i=0; $i < count($contrato) ; $i++) { 
            if ($contrato[$i]['estado'] == APP::CONT_PROCESS) {
                $dashboard['proceso']++;
            }
            if ($contrato[$i]['estado'] == APP::CONT_OK) {
                $dashboard['fin_ok']++;
            }
        }      
        //Obtenemos series por Automotora
        $series = (new DinamicView(VMAPP::aut_serie,$db))->getAutSerie($id_autom);
        if (count($series)>0) {
            $dashboard['libres'] = $series[0]['libres'];
            $dashboard['total'] = $series[0]['total'];
        }else{
            $dashboard['libres'] = 0;
            $dashboard['total'] = 0;
        }     
        
        $this->layout('layout/app');
        return new ViewModel(array('dashboard'=>$dashboard));

    }

    public function escritorioAction()
    {

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');   

        //Obtenemos datos de la sesion
        $sid = new Container('base'); 
        $usuario = $sid->offsetGet('usuario');
        $id_autom = $usuario[0]['id_automotora'];

        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContAutom($id_autom);
        $dashboard['fin_ok'] = 0;
        $dashboard['proceso'] = 0;
        for ($i=0; $i < count($contrato) ; $i++) { 
            if ($contrato[$i]['estado'] == APP::CONT_PROCESS) {
                $dashboard['proceso']++;
            }
            if ($contrato[$i]['estado'] == APP::CONT_OK) {
                $dashboard['fin_ok']++;
            }
        }
        $series = (new DinamicView(VMAPP::aut_serie,$db))->getAutSerie($id_autom);
        if (count($series)>0) {
            $dashboard['libres'] = $series[0]['libres'];
            $dashboard['total'] = $series[0]['total'];
        }else{
            $dashboard['libres'] = 0;
            $dashboard['total'] = 0;
        }  
        $result = new ViewModel(array('dashboard'=>$dashboard));
        $result->setTerminal(true);
        return $result;        
    }

}
