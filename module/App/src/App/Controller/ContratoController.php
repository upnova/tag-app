<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace App\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Application\Constants\TOASTR;
use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Application\Model\Entity\DinamicView;
use Application\Model\Entity\MarcaTable;
use Application\Model\Entity\ClaseVehTable;
use Application\Model\Entity\TagTable;
use Application\Model\Entity\PersonaTable;
use Application\Model\Entity\VehiculoTable;
use Application\Model\Entity\EstadoContratoTable;
use Application\Model\Entity\EstadoTagTable;
use Application\Model\Entity\FileContratoTable;
use Application\Model\Entity\ContratoTable;
use Application\Model\Entity\DevolucionTable;

use DOMPDFModule\View\Model\PdfModel;
use DOMPDFModule\View\Renderer\PdfRenderer;

use Zend\Session\Container;

use App\Form\TagForm;


class ContratoController extends AbstractActionController
{
    public function indexAction()
    {	
    	//Validamos sesion activa para el modulo
        $sid = new Container('base');        
        if($sid->offsetGet('urlHome') != APP::URL_APP || $sid->offsetGet('logged') != APP::LOGGED){
        	return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
        }

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');
        
        //Obtenemos id_automotora
        $usuario = $sid->offsetGet('usuario');      

        //Insertamos datos al formulario
        $form = new TagForm("form");        
        $form->get('serie')->setAttribute('options',(new DinamicView(VMAPP::tag,$db))->getTagAutom($usuario[0]['id_automotora']));
        $form->get('marca')->setAttribute('options',(new MarcaTable($db))->getComboMarca());        
        $form->get('region')->setAttribute('options',(new DinamicView(VMAPP::comuna,$db))->getComboRegion(APP::ID_CL))->setValue(13);
        $form->get('clase')->setAttribute('options',(new ClaseVehTable($db))->getComboClaseVeh())->setValue(2);

        //Retornamos a la vista
        $result = new ViewModel(array('form'=>$form));
        $result->setTerminal(true);
        return $result;    
    }

    public function nuevoAction()
    {
        try {
    
            //Conectamos con BBDD
            $db=$this->getServiceLocator()->get('Zend/Db/Adapter');

            //Obtenemos datos POST
            $data = $this->request->getPost();

            //Validamos si existe contrato para vehiculo        
            if(count((new DinamicView(VMAPP::contrato,$db))->getContrPatente($data['patente']))>0){
                return new JsonModel(array(
                    'status'=>'nok',
                    'desc'=> 'La patente ya está asociada a un contrato'
                ));
            }

            $tag = (new TagTable($db))->getTag($data['serie']);
            if ($tag[0]['id_clase'] != $data['clase']) {
                $clase = (new ClaseVehTable($db))->getClase($tag[0]['id_clase']);
                 return new JsonModel(array(
                    'status'=>'nok',
                    'desc'=> 'Clase no válida, Serie TAG válida para '.$clase[0]['clase'].' - '.$clase[0]['detalle']
                ));
            }

             //Usuario responsable
            $sid = new Container('base');
            $usuario = $sid->offsetGet('usuario');
            $data['user_create'] = $usuario[0]['id'];

            //Quitamos formato al rut        
            $rut = explode("-", $data['rut'], 2);
            $data['rut'] = str_replace('.', '', str_replace('.', '', str_replace('.', '', $rut[0]))); 
            $data['dv']  = $rut[1];              
            //Validamos existencia de persona para insertar
            $persona = (new PersonaTable($db))->getPersonaxRut($data['rut']);
            if(count($persona)>0){
                (new PersonaTable($db))->actualiza($data);
                $data['id_persona'] = $persona[0]['id'];
            }else{
                $data['id_persona'] = (new PersonaTable($db))->nuevaPersona($data);
            }        

            //Validamos si existe vehiculo para insertar        
            $clase = (new ClaseVehTable($db))->getClase($data['clase']);
            $data['tipo_vehiculo'] = $clase[0]['id_tipo_vehiculo'];
            $vehiculo = (new VehiculoTable($db))->getVehiculo($data['patente']); 
            if (count($vehiculo)>0) {
                (new VehiculoTable($db))->actualiza($data);
                $data['id_vehiculo'] = $vehiculo[0]['id'];
            }else{            
                $data['id_vehiculo'] = (new VehiculoTable($db))->nuevo($data);
            }

            //Obtenemos estado de CONTRATO para insertar        
            $est_cont = (new EstadoContratoTable($db))->getEstado(APP::CONT_PROCESS);
            $data['id_estado'] = $est_cont[0]['id'];
            $data['id_contrato'] = (new ContratoTable($db))->nuevo($data);

            //Modificamos estado del tag
            $est_tag = (new EstadoTagTable($db))->getEstado(APP::TAG_ASIGNADO);
            $data['id_estado'] = $est_tag[0]['id'];
            (new TagTable($db))->actualizaEstado($data);

            //Renombramos, asociamos al contrato y guardamos archivos adjuntos
            $adapterFile = new \Zend\File\Transfer\Adapter\Http(); 
            $files =  $this->request->getFiles()->toArray();        
            $directorio = $_SERVER['DOCUMENT_ROOT'].'/files_contrato/';        
            $adapterFile->setDestination($directorio);
            $keys = array_keys($files); // <- name inputs
            $values = array_values($files);// filename, size, tmp_folder, file_format
            for ($i=0; $i < count($files) ; $i++) {             
                if (!empty($values[$i]['name'])) {
                    $filename = $data['id_contrato'].'_'.$keys[$i].'.'.substr(basename($values[$i]['name']),-3);
                    $adapterFile->addFilter('File\Rename', array('source'=>$values[$i]['tmp_name'],'target' =>$filename,'overwrite' => true));
                    $adapterFile->receive($values[$i]['name']);
                    //Asociamos archivos al contrato
                    $data['nombre'] = $filename;
                    //Validamos que sean solo archivos del contrato
                    (new FileContratoTable($db))->nuevo($data);
                }
            }
            if ($data['devolucion'] == "si") {
                (new DevolucionTable($db))->nuevo($data);   
            }
        
            //Configuramos url para ver PDF
            $url_pdf = "app/contrato/note-pdf/".$data['id_contrato'];

            //Retornamos a la vista
            return new JsonModel(array('status'=>'ok','url_pdf'=>$url_pdf));

        } catch (\Exception $e) {
            return new JsonModel(array(
                'status'=>'nok',
                'desc'=> $e->getMessage()
            ));
        }
    }

    public function contratoPdfAction()
    {
        $pdf = new PdfModel();      
        $id_contrato = $this->params()->fromRoute('id', 0);          

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');
        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContrato($id_contrato);
        $contrato = $contrato[0];

        //Cargamos PDF Con variables de contrato        
        $pdf->setVariables(array(
            'title' => strtoupper($contrato['patente']),
            'nombre' => strtoupper($contrato['nombre'].' '.$contrato['apellido'].' '.$contrato['apellido_2']),
            'rut' => number_format($contrato['rut'],0,"",".").'-'.$contrato['dv'],
            'direccion' => $contrato['direccion'],
            'telefono' => $contrato['telefono'],
            'correo' => $contrato['correo'],
            'patente' => $contrato['patente'],
            'clase' => $contrato['numero_clase'].' - '.$contrato['detalle_clase'],
            'marca' => $contrato['marca'],
            'modelo' => $contrato['modelo'],
            'serie' => $contrato['serie'],
            'fecha' => date('d/m/Y',strtotime($contrato['fecha_inicio'])),
            'doc_electronico' => ($contrato['doc_electronico'] == 1 ? 'Sí' : 'No'),
            'nombre_titular' => '',
            'rut_titular' => '',
            ));        
        return $pdf;
    }

    public function notePdfAction()
    {
        $pdf = new PdfModel();      
        $id_contrato = $this->params()->fromRoute('id', 0);          

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');
        $contrato = (new DinamicView(VMAPP::contrato,$db))->getContrato($id_contrato);
        $contrato = $contrato[0];        

        $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
         ;
        //Cargamos PDF Con variables de contrato        
        $pdf->setVariables(array(
            'title' => strtoupper($contrato['patente']),
            'nombre' => strtoupper($contrato['nombre'].' '.$contrato['apellido'].' '.$contrato['apellido_2']),
            'rut' => number_format($contrato['rut'],0,"",".").'-'.$contrato['dv'],
            'direccion' => $contrato['direccion'],
            'telefono' => $contrato['telefono'],
            'correo' => $contrato['correo'],
            'patente' => $contrato['patente'],
            'clase' => $contrato['numero_clase'].' - '.$contrato['detalle_clase'],
            'marca' => $contrato['marca'],
            'modelo' => $contrato['modelo'],
            'serie' => $contrato['serie'],
            'fecha' => $dias[date('w',strtotime($contrato['fecha_inicio']))]." ".date('d',strtotime($contrato['fecha_inicio']))." de ".$meses[date('n',strtotime($contrato['fecha_inicio']))-1]. " de ".date('Y',strtotime($contrato['fecha_inicio'])),
            'doc_electronico' => ($contrato['doc_electronico'] == 1 ? 'Sí' : 'No'),
            'nombre_titular' => '',
            'rut_titular' => '',
            ));        
        return $pdf;
    }

    public function buscamodeloAction()
    {                                             
        //Obtenemos datos POST
        $lista = $this->request->getPost();

        //Conectamos con BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');

        //Vistas
        $ModView = new DinamicView(VMAPP::modelo,$db);
        $modelos = $ModView->getModelos($lista['id_marca']);

        //Retornamos valores a la vista        
        $result = new JsonModel($modelos);
        $result->setTerminal(true);
        return $result;                                     
    }

    public function combocomunaAction() 
    {
        $db = $this->getServiceLocator()->get('Zend\Db\Adapter');
        $DView = new DinamicView(VMAPP::comuna,$db);
        $comuna = $DView->getComboComuna($this->request->getPost('region'));

        $result = new JsonModel($comuna);
        return $result;
    }

    public function buscarutAction()
    {
        //Obtenemos ID CLIENTE from POST
        $data = $this->request->getPost();

        //Instanciamos BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');

        //Buscamos si existe persona        
        $rut = explode("-", $data['rut'], 2);
        $data['rut'] = str_replace('.', '', str_replace('.', '', str_replace('.', '', $rut[0])));         
        $persona = $this->existePersona($db,$data['rut']);

        if($persona!=false){
            $array = array('status'=>'ok','existe'=>'si','persona'=>$persona,'desc'=>TOASTR::PERSONA_ENCONTRADA);
        }else{
            $array = array('status'=>'ok','existe'=>'no','desc'=>TOASTR::PERSONA_NO_ENCONTRADA);
        }

        //Retornamos valores a la vista        
        $result = new JsonModel($array);
        $result->setTerminal(true);
        return $result; 
    }

    private function existePersona($dbAdapter,$rut)
    {
        $DView = new DinamicView(VMAPP::per,$dbAdapter); 
        $persona = $DView->getPersona($rut);
        if(count($persona)>0){
            return $persona;
        }else{
            return false;
        }
    }

    public function buscavehiculoAction()
    {
        //Obtenemos ID CLIENTE from POST
        $data = $this->request->getPost();

        //Instanciamos BBDD
        $db=$this->getServiceLocator()->get('Zend/Db/Adapter');       

        //Buscamos si existe vehiculo               
        $veh = $this->existeVehiculo($db,$data['patente']);

        if($veh!=false){
            //Buscamos si existe contrato para vehiculo 
            $contrato = (new DinamicView(VMAPP::contrato,$db))->getContrPatente($data['patente']);
            $array = array('status'=>'ok','existe'=>'si','contrato'=>$contrato,'vehiculo'=>$veh,'desc'=>TOASTR::VEH_ENCONTRADO);
        }else{
            $array = array('status'=>'ok','existe'=>'no','desc'=>TOASTR::VEH_NO_ENCONTRADO);
        }

        //Retornamos valores a la vista        
        $result = new JsonModel($array);
        $result->setTerminal(true);
        return $result; 
    }

    private function existeVehiculo($dbAdapter,$patente)
    {
        $VehTable = new VehiculoTable($dbAdapter); 
        $veh = $VehTable->getVehiculo($patente);
        if(count($veh)>0){
            return $veh;
        }else{
            return false;
        }
    }

}