<?php

namespace Application\Constants;
class TOASTR {

	//Ingresar
	const PERSONA_ENCONTRADA = "Cliente Encontrado";
	const PERSONA_NO_ENCONTRADA = "Cliente Nuevo";

	const VEH_ENCONTRADO = "Vehículo Encontrado";
	const VEH_NO_ENCONTRADO = "Vehículo Nuevo";

	const VEH_EN_TALLER = "Existe orden de trabajo abierta para esta patente";

}