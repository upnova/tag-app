<?php
/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Seditag APP
 * ///////////////////////////////////////////////////////////////////////////////
 */
namespace Application\Constants;

class VMAPP {

    //Mapeo de Vistas de la Base de Datos
    const usuario = "v_usuario";
    const acceso = "v_acceso";
    const menu = "v_menu";
    const modelo = "v_modelo";
    const comuna = "v_region_comuna";
    const autom = "v_automotora";
    const tag = "v_tag";
    const per = "v_persona";
    const contrato = "v_contrato";
    const dash = "v_adm_dashboard";
    const cont_mes = "v_contratos_mes";
    const aut_serie = "v_autom_series";
    const carga_tag = "v_carga_tag";
    
    
}


