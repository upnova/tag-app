<?php
/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Becheck
 * ///////////////////////////////////////////////////////////////////////////////
 */
namespace Application\Constants;
class APP {
    
    //API-KEYS
    //Mandrill
    const KEY_MANDRILL = "";

    //Countries in DB
    const ID_CL = 43;

    //ESTADOS TAGS Y CONTRATOS
    const TAG_ASIGNADO = "ASIGNADO";
    const TAG_LIBRE = "LIBRE";
    const CONT_OK = "FIN OK";
    const CONT_NOK = "CANCELADA";
    const CONT_PROCESS = "EN PROCESO";

    //Modulos
    const URL_APP = "app";
    const URL_ADMIN = "admin";
    const URL_EMPTY="";
    const URL_HOME="http://app.seditag.cl";

    //Constantes de Logeo
    const LOGGED = "y";
    const NOT_LOGGED = "not";

    const ACTION_SEND="send";
    const ACTION_ERROR="error";

    const ERROR_EMPTY ="";

    //Tipo Estado
    const STATUS_OK="ok";
    const STATUS_NOK="nok";  

    //URL
    const URL_AVATAR = "/img/avatar/";
    const URL_AVATAR_NOT_FOUND = "/img/avatar/user-icon.png";
    const URL_FILES = "/files_contrato/";
   
    //Constante de Mensaje de Error Dinamico
    private static $error_msg =array(
        0=>"No tiene permiso",
        1=>"El usuario y/o password son incorrectos",    
        );

    public static function ERROR_LOGIN($index = false) {
        return $index !== false ? self::$error_msg[$index] : self::$error_msg;
    }
}
?>
