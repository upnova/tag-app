<?php
namespace Application\Constants;
class RTN {

   const NOT_FOUND = "application/index/nofound";
   
   //Se matricula las rutas
   public static function routing() {
        
        $map =array(
            //Modulo Automotora
            "app"=>"app",
            "admin"=>"admin",
           
           //Modulo Seditag
            "carga"=>"serie",
            "contrato"=>"contrato",
            "escritorio"=>"index/escritorio",
            "alta"=>"alta",

            //Modulo Automotora            
        );
        
        return $map;
        
    }
}

