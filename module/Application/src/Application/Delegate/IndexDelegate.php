<?php

/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Becheck
 * ///////////////////////////////////////////////////////////////////////////////
 */
namespace Application\Delegate;
use Application\Constants\APP;
use Application\Constants\RTN;
use Application\Constants\VMAPP;
use Application\Model\Entity\DinamicView;
use Application\Delegate\SesionDelegate;

class IndexDelegate extends SesionDelegate{
    
    public function getUrl($datos){
        
        $url=APP::URL_EMPTY;
        $raiz="";
        $acceso = $this->getSesion('acceso');

        if(isset($acceso)){
           
            $map = RTN::routing();
            // encontramos el nombre verdadero
            foreach( $map as $clave=>$valor){
                 if($acceso[0]['ruta_modulo']==$clave){
                     $raiz=$valor;
                     $acceso[0]['ruta_true'] = $valor;
                     $this->setSesion('acceso', $acceso);
                     $this->setSesion('url_sesion', $raiz);
                     break;
                 }
            }

            //encontrando menu
                foreach( $map as $clave=>$valor){
                    if($datos['data']==$clave){
                        $url=$raiz.'/'.$valor;
                        
                        break;
                    }
                }
                        
        }
        if($url==APP::ERROR_EMPTY){
            $url=RTN::NOT_FOUND;
        }
        return  array('url'=>$url,'url_completa'=>false);

    }
    
    public function getTorre($dbAdapter,$id_condominio)
    {
        $torre = new TorreTable($dbAdapter);
        if(count($torre)==0){
            throw new \Exception(APP::ERROR_LOGIN(20));
        }
        return $torre->getTorreByCondominio($id_condominio);
    } 

    public function set_change_module(){
        
        $modulos = $this->getSesion('modulos');
        $modulo = $this->getSesion('modulo');
        if(isset($modulos) && isset($modulo)){
        for($y=0;$y<count($modulos);$y++){
            if($modulo['id']==$modulos['lstModule'][$y]['id']){
                $this->setSesion('change_module', APP::FLAG_SI);
                break;
            }
        }
    }
    }
    
    public function getTemplateUnits($dbAdapter){
        $id_cnd = $this->getSesion('condominio')[0]['id_condominio'];
        $lista = (new TorreTable($dbAdapter))->obtenerTorres($id_cnd);
        if(count($lista)>0){
            return APP::TEMPLATE_UNIDADES_SUB;
        }else{
            return APP::TEMPLATE_UNIDADES;
        }
        
    }
    public function generateMedidorTemplate($dbAdapter,$origenFile,$destinoFile){
        
        $id_cnd = $this->getSesion('condominio')[0]['id_condominio'];
        $cnd = $this->getSesion('condominio')[0]['nombre'];
        $periodo = (new PeriodoTable($dbAdapter))->getPeriodo($id_cnd);
        $datosAguaCaliente = (new DinamicView(VMAPP::medidor, $dbAdapter))->
        getMedidorNombre($id_cnd, APP::MEDIDOR_AGUA, $periodo);
        $datosCalefaccion = (new DinamicView(VMAPP::medidor, $dbAdapter))->
        getMedidorNombre($id_cnd, APP::MEDIDOR_CALEFACCION, $periodo);
        
        if (count($datosAguaCaliente)==0 && count($datosCalefaccion) ==0){
            throw new \Exception(APP::ERROR_LOGIN(35));
        }
        
        (new FileOperation())->rewriteMedidorExcel($origenFile, $destinoFile, $datosAguaCaliente, $datosCalefaccion,$cnd);
        
    }
    
    public function generateUnitTemplate($dbAdapter,$origenFile,$destinoFile){
        
        $id_cnd = $this->getSesion('condominio')[0]['id_condominio'];
        $cnd = $this->getSesion('condominio')[0]['nombre'];
        
        $torres = (new TorreTable($dbAdapter))->getTorreByCondominio($id_cnd);
        if (count($torres)==0){
            throw new \Exception("No hay registro de condominio en torre");
        }
        $subtorres = (new TorreTable($dbAdapter))->getTorreByCondominioAndSubCnd($id_cnd, 1);
        
        (new FileOperation())->rewriteUnitExcel($origenFile, $destinoFile, $torres,$subtorres,$cnd);
        
    }
}
