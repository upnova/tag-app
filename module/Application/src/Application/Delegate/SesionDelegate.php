<?php

/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Becheck
 * ///////////////////////////////////////////////////////////////////////////////
 */

namespace Application\Delegate;
use Zend\Session\Container;

class SesionDelegate {
    
    public function setSesion($key,$value){
        $sid_adm =  new Container('base');
        $sid_adm->offsetSet($key, $value);
    }
    public function getSesion($key){
        $sid_adm =  new Container('base');
        if($sid_adm->offsetExists($key)){
            return $sid_adm->offsetGet($key);
        }else{
           return null;
        }
        
    }
    public function exist($key){
        $sid_adm =  new Container('base');
        if($sid_adm->offsetExists($key)){
            return true;
        }else{
           return false;
        }
    }
    
    public function clearSesion(){
       $sid_adm = new Container('base');
       $sid_adm->getManager()->getStorage()->clear();
      // $sid_adm->getManager()->expireSessionCookie();
       /*foreach ($_COOKIE as $key => $value)
       {
            unset($value);
            setcookie($key, '', time() - 3600);
        }*/
    }
    
}