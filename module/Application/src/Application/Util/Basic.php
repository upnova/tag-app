<?php
namespace Application\Util;

class Basic{


  public static function generateSessionId()
  {
    $caracteres = "abcdefghijklmnopqrstuvwxyz0123456789";
    $tam =strlen($caracteres);
    $cad_aleatoria="";
    for($i=0;$i<25;$i++)
      $cad_aleatoria = $cad_aleatoria.substr($caracteres,rand(0,$tam),1);
    return $cad_aleatoria;
  }

  public static function expireDate($fecha){

    if($fecha == null || $fecha==""){
      return true;
    }

    $time = time();
    $diff = strtotime($fecha) - strtotime(date("Y-m-d H:i:s", $time));
    if($diff>0){
      return true;
    }else{
      return false;
    }

  }

 public static function vistaMenu($myArray){

  $completo = Array();
  $i=0;
  foreach($myArray as $datos){
            //como la informacion llega ordenado por el id_padre
            //se llenaran los primeros los padres
    if($datos['id_padre']==0){
      $completo[$i]=array('nombre'=>$datos['nombre'],'ruta'=>$datos['ruta'],'icono'=>$datos['icono'],'submenu'=>array());
      $i++;
    }

  }
  return $completo;
}

public static function validaRut($rut){
  if(strpos($rut,"-")===false){
    return false;
  }
  if(strpos($rut,"-")==false){
    $RUT[0] = substr($rut, 0, -1);
    $RUT[1] = substr($rut, -1);
  }else{$RUT = explode("-", trim($rut));}
  $elRut = str_replace(".", "", trim($RUT[0]));
  $factor = 2;$suma=0;
  for($i = strlen($elRut)-1; $i >= 0; $i--):
    $factor = $factor > 7 ? 2 : $factor;
  $suma += $elRut{$i}*$factor++;
  endfor;
  $resto = $suma % 11;
  $dv = 11 - $resto;
  if($dv == 11){$dv=0;
  }else if($dv == 10){$dv="k";
}else{$dv=$dv;}
if($dv == trim(strtolower($RUT[1]))){
 return true;
}else{
 return false;
}
}

public static function obtenerDV ($rut_sin_dv){
    
    /* Bonus: remuevo los ceros del comienzo. */
    while($rut_sin_dv[0] == "0") {
        $rut_sin_dv = substr($rut_sin_dv, 1);
    }
    $factor = 2;
    $suma = 0;
    for($i = strlen($rut_sin_dv) - 1; $i >= 0; $i--) {
        $suma += $factor * $rut_sin_dv[$i];
        $factor = $factor % 7 == 0 ? 2 : $factor + 1;
    }
    $dv = 11 - $suma % 11;
    /* Por alguna razón me daba que 11 % 11 = 11. Esto lo resuelve. */
    $dv = $dv == 11 ? 0 : ($dv == 10 ? "K" : $dv);
    return  $dv;

}


public static function reorganizateMenu($menus){
  $salida = array();
  $cardinal = count($menus);
  $k=0;
  $act=0;
  $t=0;
  for($i=0;$i<$cardinal;$i++){
    if($i==0){
      $id_mi = $menus[$i]['id_menu'];
      $salida[$k]['padre'] = array('id_menu'=>$menus[$i]['id_menu'],'nombre_menu'=>$menus[$i]['nombre_menu'],'ruta_menu'=>$menus[$i]['ruta_menu'],'icono_menu'=>$menus[$i]['icono_menu']);
      $act=$k;
      $t=0;
      $k++;
    }
    if($id_mi!=$menus[$i]['id_menu']){
      $id_mi = $menus[$i]['id_menu'];
      $salida[$k]['padre'] = array('id_menu'=>$menus[$i]['id_menu'],'nombre_menu'=>$menus[$i]['nombre_menu'],'ruta_menu'=>$menus[$i]['ruta_menu'],'icono_menu'=>$menus[$i]['icono_menu']);
      $act=$k;
      $t=0;
      $k++;
    }

    if(isset($menus[$i]['id_submenu'])){

      $salida[$act]['hijo'][$t] = array('id_submenu'=>$menus[$i]['id_submenu'],'nombre_submenu'=>$menus[$i]['nombre_submenu'],'ruta_submenu'=>$menus[$i]['ruta_submenu'],'icono_submenu'=>$menus[$i]['icono_submenu']);
      $t++;
    }

  }

  return $salida;


}

public static function deleteDuplicateArrays($array,$key)
{

  $temp_array = array();
  foreach ($array as &$v) {
   if (!isset($temp_array[$v[$key]]))
     $temp_array[$v[$key]] =& $v;
 }
 $array = array_values($temp_array);

 return $array;

}

public static function getMeses()
{

  $meses = array(
    '01'=>'Enero',
    '02'=>'Febrero',
    '03'=>'Marzo',
    '04'=>'Abril',
    '05'=>'Mayo',
    '06'=>'Junio',
    '07'=>'Julio',
    '08'=>'Agosto',
    '09'=>'Septiembre',
    '10'=>'Octubre',
    '11'=>'Noviembre',
    '12'=>'Diciembre',
    );

  return $meses;

}

public static function getMes($mes)
{
  if(!isset($mes) || $mes == ""){
      return "";
  }
  $mes = (int) $mes;
  $meses = array(
    1=>'Enero',
    2=>'Febrero',
    3=>'Marzo',
    4=>'Abril',
    5=>'Mayo',
    6=>'Junio',
    7=>'Julio',
    8=>'Agosto',
    9=>'Septiembre',
    10=>'Octubre',
    11=>'Noviembre',
    12=>'Diciembre');

  return $meses[$mes];

}

public static function getMesCorto($mes)
{
  if(!isset($mes) || $mes == ""){
      return "";
  }
  $mes = (int) $mes;
  $meses = array(
    1=>'Ene',
    2=>'Feb',
    3=>'Mar',
    4=>'Abr',
    5=>'May',
    6=>'Jun',
    7=>'Jul',
    8=>'Ago',
    9=>'Sep',
    10=>'Oct',
    11=>'Nov',
    12=>'Dic');

  return $meses[$mes];

}

public static function getMesAbreviado($mes)
{
  $mes = (int) $mes;
  $meses = array(
    1=>'Ene',
    2=>'Feb',
    3=>'Mar',
    4=>'Abr',
    5=>'May',
    6=>'Jun',
    7=>'Jul',
    8=>'Ago',
    9=>'Sep',
    10=>'Oct',
    11=>'Nov',
    12=>'Dic');

  return $meses[$mes];

}
public static function getNumbersSeconds($sum,$fechaInicial=null)
{
 $segundos = 0;
 $diff = date('n')+$sum;
 if($diff > 12){
   $year = date('Y')+1;
   $month = $diff-12;
   $dia = cal_days_in_month(CAL_GREGORIAN,$diff-12, date('Y'),$year);                    
 }else{
   $year = date('Y');
   $month = $diff;
   $dia = cal_days_in_month(CAL_GREGORIAN,$diff,$year);                
 }
 $fechaFinal = $dia.'-'.$month.'-'.$year;     

 if(isset($fechaInicial) && $fechaInicial<>''){
   $segundos = strtotime($fechaFinal) - strtotime($fechaInicial);
 }else{
   $time = time();
   $segundos = strtotime($fechaFinal) - strtotime(date("Y-m-d H:i:s", $time));
 }
 return $segundos;
}

public static function convertDateYMD($fecha)
{
  return date("Y-m-d", strtotime($fecha));
}

public static function convertDateDMY($fecha)
{
  return date("d-m-Y", strtotime($fecha));
}

public static function getDiaMesAnioSlash($fecha=null){

 $retorna = '';

 if(isset($fecha)&&$fecha<>''){
  if (strpos($fecha, '-') !== FALSE){
    $frmt = "Y/m/d";
  }
  if (strpos($fecha, '/') !== FALSE){
    $frmt = "d/m/Y";
  }
  if (strpos($fecha, ' ') !== FALSE){
    $frmt = "d m Y";
  }
  $date = \DateTime::createFromFormat($frmt, $fecha);
          $retorna= $date;//$date->format('d-m-Y');

        }else{
         $time = time();
         $retorna = date("d/m/Y",$time);
       }
       return $retorna;
     }

     public static function getDiaMesAnioHoraMinSegSlash($fecha=null){

       $retorna = '';
       
       if(isset($fecha)&&$fecha<>''){
        if (strpos($fecha, '-') !== FALSE){
          $frmt = "Y-m-d H:i:s";
        }
        if (strpos($fecha, '/') !== FALSE){
          $frmt = "d/m/Y H:i:s";
        }
        $date = \DateTime::createFromFormat($frmt, $fecha);
        $retorna=$date->format('d/m/Y H:i:s');

      }else{
       $time = time();
       $retorna = date("d/m/Y H:i:s",$time);
     }
     return $retorna;
   }   

   public static function getDiaNombreMesAnio($fecha=null){

     $retorna = '';

     if(isset($fecha)&&$fecha<>''){
      if (strpos($fecha, '-') !== FALSE){
        $frmt = "Y-m-d";
      }
      if (strpos($fecha, '/') !== FALSE){
        $frmt = "d/m/Y";
      }
      if (strpos($fecha, ' ') !== FALSE){
        $frmt = "d m Y";
      }
      $date = \DateTime::createFromFormat($frmt, $fecha);
      $fecN=$date->format('d-m-Y');
      $nombreMes = self::getMes(date("m", strtotime($fecN)));
      $retorna = str_replace("-".date("m", strtotime($fecN))."-", " ".$nombreMes." ", $fecN);;

    }else{
     $time = time();
     $nombreMes = self::getMes(date('m'));
     $retorna = str_replace("/".date('m')."/", " ".$nombreMes." ", date("d/m/Y",$time));

   }
   return $retorna;
 }

 public static function getDiaNombreAbreviadoMesAnio($fecha=null){

   $retorna = '';

   if(isset($fecha)&&$fecha<>''){
    if (strpos($fecha, '-') !== FALSE){
      $frmt = "Y-m-d";
    }
    if (strpos($fecha, '/') !== FALSE){
      $frmt = "d/m/Y";
    }
    if (strpos($fecha, ' ') !== FALSE){
      $frmt = "d m Y";
    }
    $date = \DateTime::createFromFormat($frmt, $fecha);
    $fecN=$date->format('d-m-Y');
    $nombreMes = self::getMesAbreviado(date("m", strtotime($fecN)));
    $retorna = str_replace("-".date("m", strtotime($fecN))."-", "-".$nombreMes."-", $fecN);;

  }else{
   $time = time();
   $nombreMes = self::getMesAbreviado(date('m'));
   $retorna = str_replace("/".date('m')."/", "-".$nombreMes."-", date("d/m/Y",$time));

 }
 return $retorna;
}

public static function getDiaNombreMesAnioHoraMinSeg($fecha=null){

 $retorna = '';

 if(isset($fecha)&&$fecha<>''){
  if (strpos($fecha, '-') !== FALSE){
    $frmt = "Y-m-d H:i:s";
  }
  if (strpos($fecha, '/') !== FALSE){
    $frmt = "d/m/Y H:i:s";
  }

  $date = \DateTime::createFromFormat($frmt, $fecha);
  $fecN=$date->format('d-m-Y H:i:s');
  $nombreMes = self::getMes(date("m", strtotime($fecN)));
  $retorna = str_replace("-".date("m", strtotime($fecN))."-", " ".$nombreMes." ", $fecN);;

}else{
 $time = time();
 $nombreMes = self::getMes(date('m'));
 $retorna = str_replace("/".date('m')."/", " ".$nombreMes." ", date("d/m/Y H:i:s",$time));

}
return $retorna;
}

public static function getDiaNombreAbreviadoMesAnioHoraMinSeg($fecha=null){

 $retorna = '';

 if(isset($fecha)&&$fecha<>''){
  if (strpos($fecha, '-') !== FALSE){
    $frmt = "Y-m-d H:i:s";
  }
  if (strpos($fecha, '/') !== FALSE){
    $frmt = "d/m/Y H:i:s";
  }
  $date = \DateTime::createFromFormat($frmt, $fecha);
  $fecN=$date->format('d-m-Y H:i:s');
  $nombreMes = self::getMesAbreviado(date("m", strtotime($fecN)));
  $retorna = str_replace("-".date("m", strtotime($fecN))."-", "-".$nombreMes."-", $fecN);;

}else{
 $time = time();
 $nombreMes = self::getMesAbreviado(date('m'));
 $retorna = str_replace("/".date('m')."/", "-".$nombreMes."-", date("d/m/Y H:i:s",$time));

}
return $retorna;
}

}



