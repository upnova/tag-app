<?php

namespace Application\Util;

class Encr{
    
    public static function convertPass($user,$pass){
        return substr(strrev($pass),0,24).substr(md5(substr($user,0,1).substr($pass,0,1)),0,8);
    }

     public static function getPass($user,$pass){
        return substr(md5($pass),0,24).substr(md5(substr($user,0,1).substr(md5($pass),31,1)),0,8);
    }
 
 }
?>