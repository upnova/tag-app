<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class VehiculoTable extends TableGateway {
    
	private $patente;
	private $id_tipo_vehiculo;
	private $id_marca;
	private $id_modelo;
    private $kilometraje;
	private $year;
	private $chasis;
   

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {

        return parent::__construct('ng_vehiculo', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->patente = $datos["patente"];
        $this->id_tipo_vehiculo = $datos["tipo_vehiculo"];
        $this->id_marca = $datos["marca"];
        $this->kilometraje = isset($datos["kilometraje"])?$datos["kilometraje"]:null;
        $this->id_modelo = $datos["modelo"];
        $this->year = isset($datos["year"])?$datos["year"]:null;
        $this->chasis = isset($datos["chasis"])?$datos["chasis"]:null;
    }

    public function nuevo($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'patente' => strtoupper($this->patente), 
            'id_tipo_vehiculo' => $this->id_tipo_vehiculo,
            'id_marca' => $this->id_marca,
            'kilometraje' => $this->kilometraje,
            'id_modelo' => $this->id_modelo,
            'year' => $this->year,
        );

        $this->insert($array);
        return $this->lastInsertValue;        
    }

    public function getVehiculo($patente)
    {
        return $this->select(array('patente' => $patente))->toArray();
    }  

    public function actualiza($data = array()) {

        self::cargarCampos($data);
        $array = array
            (
            'patente' => $this->patente,
            'id_tipo_vehiculo' => $this->id_tipo_vehiculo,
            'id_marca' => $this->id_marca,
            'id_modelo' => $this->id_modelo,
            'year' => $this->year,
        );

        $this->update($array, array('patente' => $data->patente));
        return true;
    }
}
