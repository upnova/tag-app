<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class PersonaTable extends TableGateway {
    
	private $rut;
	private $dv;
	private $dni;
	private $tipo;
	private $nombre;
	private $apellido;
	private $apellido_2;
	private $telefono;
	private $telefono_2;
	private $direccion;
	private $id_comuna;	
	private $correo;
	private $correo_2;
	private $foto;
	private $observacion;
	private $fecha_nacimiento;
    

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {

        return parent::__construct('persona', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->rut = isset($datos["rut"])?$datos["rut"]:null;
        $this->dv = isset($datos["dv"])?$datos["dv"]:null;
        $this->dni = isset($datos["dni"]) ? $datos["dni"] : null;
        $this->tipo = isset($datos["tipo_persona"]) ? $datos["tipo_persona"] : "natural";
        $this->nombre = isset($datos["nombre"]) ? $datos["nombre"] : null;
        $this->apellido = isset($datos["apellido"]) ? $datos["apellido"] : null;
        $this->apellido_2 = isset($datos["apellido2"]) ? $datos["apellido2"] : null;
        $this->telefono = isset($datos["telefono"]) ? $datos["telefono"] : null;
        $this->telefono_2 = isset($datos["telefono_2"]) ? $datos["telefono_2"] : null;
        $this->direccion = isset($datos["direccion"]) ? $datos["direccion"] : null;
        //no tocar el id_comuna
        $this->id_comuna = isset($datos["comuna"]) ? $datos["comuna"]=="null"?null:$datos["comuna"] : null;        
        $this->foto = isset($datos["foto"]) ? $datos["foto"] : null;                
        $this->fecha_nacimiento = isset($datos["fecha_nacimiento"]) ? $datos["fecha_nacimiento"] : null;        
        $this->correo = isset($datos["correo"]) ? $datos["correo"] : null;
        $this->correo_2 = isset($datos["correo_2"]) ? $datos["correo_2"] : null;
        $this->observacion = isset($datos["observacion"]) ? $datos["observacion"] : null;        
    }

    public function nuevaPersona($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'rut' => $this->rut,
            'tipo' => $this->tipo,
            'dv' => $this->dv,                        
            'nombre' => ucwords(mb_strtolower($this->nombre)),
            'apellido' => ucwords(mb_strtolower($this->apellido)),
            'apellido_2' => ucwords(mb_strtolower($this->apellido_2)),
            'telefono' => $this->telefono,        
            'direccion' => $this->direccion,
            'id_comuna' => $this->id_comuna,
            'correo' => $this->correo,            
        );

        $this->insert($array);
        $id = $this->lastInsertValue;
        return $id;
    }

    public function getPersonaxId($id) {
        return $this->select(array('id' => $id))->toArray();
    }

    public function getPersonaxRut($rut) {
        return $this->select(array('rut' => $rut))->toArray();
    }
    public function getPersonaxCorreo($correo){
        
        $datos = $this->select(function (Select $select) use ($correo) {
            $where = new Where();
            $where->expression('lower(correo) = ?', strtolower($correo));
            $select->where($where);
        });
        return $datos->toArray();
        
        //return $this->select(array('correo' => $correo))->toArray();
    }

    public function actualiza($data = array()) {

        self::cargarCampos($data);
        $array = array
            (
            'rut' => $this->rut,
            'dv' => $this->dv,                        
            'nombre' => ucwords(mb_strtolower($this->nombre)),
            'apellido' => ucwords(mb_strtolower($this->apellido)),
            'apellido_2' => ucwords(mb_strtolower($this->apellido_2)),
            'telefono' => $this->telefono,            
            'direccion' => $this->direccion,
            'id_comuna' => $this->id_comuna,            
            'correo' => $this->correo,  
        );

        $this->update($array, array('rut' => $data->rut));

        return true;
    }

    public function getPersonaxRutLike($rut) {
        $datos = $this->select(function (Select $select) use ($rut) {
            $where = new Where();
            $where->like('rut', $rut . "%");
            $select->where($where);
        });
        $recorre = $datos->toArray();
        return $recorre;
    }

    public function getPersonaxNombreLike($nombre) {
        $datos = $this->select(function (Select $select) use ($nombre) {
            $where = new Where();
            $where->expression('upper(concat(replace(nombre," ",""),apellido)) LIKE ?', "%" . strtoupper($nombre) . "%");
            $select->where($where);
        });
        $recorre = $datos->toArray();
        return $recorre;
    }

    public function getDatosRut($rut)
    {
        //$datos = $this->select(array('rut'=>$rut));
        $datos = $this->select(function (Select $select) use ($rut) {
        $where = new Where();
        $where->like('rut', $rut."%"); 
        $select->where($where);
    });        
        $recorre = $datos->toArray();
        return $recorre;
    }

    public function existRutPerson($rut_sin_dv){
        if(count($this->select(array('rut'=>$rut_sin_dv)))>0){
            return true;
        }else{
            return false;
        }
    }

}
