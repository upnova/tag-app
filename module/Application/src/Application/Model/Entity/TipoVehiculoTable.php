<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class TipoVehiculoTable extends TableGateway
{    
    private $dbAdapter;

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('ng_tipo_vehiculo', $adapter, $databaseSchema,$selectResultPrototype);
    }

    public function getComboTipoVehiculo() {

        $result = $this->select()->toArray();
        
        $resultado[""] = "";            
        for($i=0;$i<count($result);$i++)
        {
            $resultado[$result[$i]['id']] = $result[$i]['nombre']; 
        }                
        return $resultado;
    }   
}