<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class FileContratoTable extends TableGateway {
    
	private $nombre;
	private $id_contrato;
   

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('ng_file_contrato', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->nombre = $datos["nombre"];
        $this->id_contrato = $datos["id_contrato"];
    }

    public function nuevo($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'nombre' => $this->nombre, 
            'id_contrato' => $this->id_contrato,
        );

        $this->insert($array);
    }

    public function getFiles($id_contrato)
    {
        return $this->select(array('id_contrato' => $id_contrato))->toArray();
    }  
}
