<?php
/* 
 * ////////////////////////////////////////////////////////////////////////////////
 * //  Copyright (c) 2016 Upnova - Todos los Derechos Reservados.
 * //  http://www.upnova.cl/
 * //  Autor: Grupo de Desarrollo de Upnova
 * //  Proyecto: Seditag APP
 * ///////////////////////////////////////////////////////////////////////////////
 */
namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Application\Constants\APP;
use Application\Util\Encr;

class DinamicView extends TableGateway {

    public function __construct($vista = null, Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct($vista, $adapter, $databaseSchema, $selectResultPrototype);
    }

    /////////////////////////////
    //v_dashboard
    /////////////////////////////
    public function getDash() 
    {
        return $this->select()->toArray();
    }

    /////////////////////////////
    //v_contratos_mes
    /////////////////////////////
    public function getContMes() 
    {
        return $this->select()->toArray();
    }

    /////////////////////////////
    //v_carga_tag
    /////////////////////////////
    public function getCarga() 
    {
        return $this->select()->toArray();
    }

    public function getCargaId($id_carga) 
    {
        return $this->select(array('id'=>$id_carga))->toArray();
    }    

    /////////////////////////////
    //v_autom_series
    /////////////////////////////
    public function getAutSerie($id_autom) 
    {
        return $this->select(array('id_automotora'=>$id_autom))->toArray();
    }

    public function getSeriesTotales() 
    {
        return $this->select()->toArray();
    }
    
    //////////////////////////////
    //v_persona
    /////////////////////////////
    public function getPersona($rut) 
    {
        return $this->select(array('rut' => $rut))->toArray();
    }

    //////////////////////////////
    //v_usuario
    /////////////////////////////
    public function getUsuario($user,$pass) 
    {
        return $this->select(array('nombre' => $user, 'clave' => Encr::convertPass($user,$pass)))->toArray();
    }

    //////////////////////////////
    //v_acceso
    /////////////////////////////
     public function getAcceso($id_usuario) 
    {
        return $this->select(array('id_usuario' => $id_usuario))->toArray();
    }

    //////////////////////////////
    //v_menu
    /////////////////////////////
     public function getMenu($id_pm) 
    {
        return $this->select(array('id_perfil_modulo' => $id_pm))->toArray();
    }

    //////////////////////////////
    //v_region_comuna
    /////////////////////////////
    public function getComboComuna($id_region)
    {
        $datos = $this->select(function (Select $select) use ($id_region)  {
            $where = new Where();            
            $where->expression('id_region = ?', $id_region);       
            $select->where($where);            
            $select->order('nombre_comuna asc');             
        });
        $recorre = $datos->toArray();
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['id_comuna']] = $recorre[$i]['nombre_comuna'];
        }
         return $resultado;
    }

    public function getComboRegion($id_pais)
    {
        $datos = $this->select(function (Select $select) use ($id_pais)  {
            $where = new Where();            
            $where->expression('id_pais = ?', $id_pais);      
            $select->columns(array(new Expression('DISTINCT(nombre_region) as nombre_region,id_region')));
            $select->where($where);            
            $select->order('id_region asc');             
        });
        $recorre = $datos->toArray();
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['id_region']] = $recorre[$i]['nombre_region'];
        }
         return $resultado;
    }

    public function getComboComAll()
    {
        $datos = $this->select(function (Select $select) {
            $where = new Where();
            $select->where($where);            
            $select->order('nombre_comuna asc');             
        });
        $recorre = $datos->toArray();
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['id_comuna']] = $recorre[$i]['nombre_comuna'];
        }
         return $resultado;
    }

    //////////////////////////////
    //v_modelo
    /////////////////////////////
    public function getModelos($id_marca=null) 
    {
        if(isset($id_marca)){
            return $this->select(array('id_marca' => $id_marca))->toArray();
        }else{
            return $this->select()->toArray();
        }
    }

    public function getModelo($id_modelo) 
    {
        return $this->select(array('id_modelo' => $id_modelo))->toArray();
    }

    //////////////////////////////
    //v_automotora
    /////////////////////////////
    public function getComboAutom() 
    {
        $recorre = $this->select()->toArray();
        $resultado[''] = "Seleccione Automotora";
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['id']] = $recorre[$i]['nombre'];
        }
        return $resultado;
    }

    //////////////////////////////
    //v_tag
    /////////////////////////////
    public function getTag() 
    {
        return $this->select()->toArray();
    }

    public function getTagAutom($id_autom) 
    {
        $recorre = $this->select(array('id_automotora' => $id_autom,'estado'=>APP::TAG_LIBRE))->toArray();

        $resultado['']="Seleccione una serie";
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['serie']] = $recorre[$i]['serie'];
        }
        return $resultado;
    }

    public function getCountAutom($id_autom) 
    {
        return count($this->select(array('id_automotora' => $id_autom,'estado'=>APP::TAG_LIBRE))->toArray());
    }


    //////////////////////////////
    //v_contrato
    /////////////////////////////
    public function getContrPatente($patente) 
    {
        return $this->select(function (Select $select) use ($patente)  {
            $where = new Where();            
            $where->expression('patente = ?', $patente);
            $where->expression('estado <> ?', APP::CONT_NOK);            
            $select->where($where);            
        })->toArray();        
    }

    public function getContrato($id)
    {
        return $this->select(array('id'=>$id))->toArray();
    }

    public function getContAutom($id_autom)
    {
        return $this->select(array('id_automotora'=>$id_autom))->toArray();
    }

    public function getContAll()
    {
        return $this->select()->toArray();
    }

}
