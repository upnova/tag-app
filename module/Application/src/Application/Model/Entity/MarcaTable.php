<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class MarcaTable extends TableGateway {

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('ng_marca', $adapter, $databaseSchema, $selectResultPrototype);
    }


    public function getComboMarca() {
        $datos = $this->select();

        $recorre = $datos->toArray();
        $resultado[''] = 'Buscador de Marcas...';
        for ($i = 0; $i < count($recorre); $i++) {
            $resultado[$recorre[$i]['id']] = $recorre[$i]['nombre'];
        }

        return $resultado;
    }

}
