<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class CargaTagTable extends TableGateway
{    
    private $id_clase;    
    private $user_create;

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('ng_carga_tag', $adapter, $databaseSchema,$selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->id_clase = $datos["clase"];        
        $this->user_create = $datos["user_create"];
    }

    public function nuevo($data = array())
    {
        self::cargarCampos($data);
        $array = array
        (
            'id_clase' => $this->id_clase,             
            'user_create' => $this->user_create
        );

        $this->insert($array);
        return $this->lastInsertValue;
    }

    public function getCarga($id) 
    {
        return $this->select(array('id'=>$id))->toArray();
    } 

}