<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class ContratoTable extends TableGateway {
    
	private $id_vehiculo;
	private $id_persona;
	private $serie_tag;
    private $id_clase;
	private $id_estado;
	private $fecha_inicio;
    private $doc_electronico;
	private $user_create;
    private $observacion;
   

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('ng_contrato', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->id_vehiculo = $datos["id_vehiculo"];
        $this->id_persona = $datos["id_persona"];
        $this->serie_tag = $datos["serie"];
        $this->id_clase = $datos["clase"];
        $this->id_estado = $datos["id_estado"];
        $this->fecha_inicio = $datos["fecha"];
        $this->doc_electronico = isset($datos["doc_electronico"])?1:0;
        $this->user_create = $datos["user_create"];
        $this->observacion = isset($datos["observacion"]) ? $datos["observacion"] : 0;
    }

    public function nuevo($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'id_vehiculo' => $this->id_vehiculo, 
            'id_persona' => $this->id_persona,
            'serie_tag' => $this->serie_tag,
            'id_clase' => $this->id_clase,
            'id_estado' => $this->id_estado,
            'fecha_inicio' => $this->fecha_inicio,
            'doc_electronico' => $this->doc_electronico,
            'user_create' => $this->user_create,
            'observacion' => $this->observacion,
        );

        $this->insert($array);
        return $this->lastInsertValue;
    }

    public function actualizaEstado($id_estado,$id)
    {        
        $this->update(array('id_estado' => $id_estado), array('id' => $id));
        return true;
    }

    public function marcaContrato($id)
    {        
        $this->update(array('observacion' => '1'), array('id' => $id));        
    }
}
