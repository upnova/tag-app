<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class EstadoContratoTable extends TableGateway
{    
    private $dbAdapter;

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('ng_estado_contrato', $adapter, $databaseSchema,$selectResultPrototype);
    }

    public function getEstado($nombre) 
    {
		return $this->select(array('nombre'=>$nombre))->toArray();		
    }   
}