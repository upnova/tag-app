<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class DevolucionTable extends TableGateway {
    
	private $serie;	
	private $id_contrato;
   

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('ng_devolucion', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->serie = $datos["serie_devolucion"];        
        $this->id_contrato = $datos["id_contrato"];
    }

    public function nuevo($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'serie' => $this->serie,                
            'id_contrato' => $this->id_contrato,
        );

        $this->insert($array);
    }
}
