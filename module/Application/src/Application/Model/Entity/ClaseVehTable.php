<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;

class ClaseVehTable extends TableGateway
{    
    private $dbAdapter;

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null)
    {
        return parent::__construct('ng_clase_vehiculo', $adapter, $databaseSchema,$selectResultPrototype);
    }

    public function getComboClaseVeh() {

        $result = $this->select()->toArray();
        
        $resultado[""] = "";            
        for($i=0;$i<count($result);$i++)
        {
            $resultado[$result[$i]['id']] = $result[$i]['nombre'].' - '.$result[$i]['detalle']; 
        }                
        return $resultado;
    }

    public function getClase($id) 
    {
        return $this->select(array('id'=>$id))->toArray();
    }   
}