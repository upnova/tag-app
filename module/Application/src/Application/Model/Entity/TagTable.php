<?php

namespace Application\Model\Entity;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;

class TagTable extends TableGateway {
    
	private $serie;
	private $id_automotora;
    private $id_clase;   
    private $id_carga;
    private $id_estado;
   

    public function __construct(Adapter $adapter = null, $databaseSchema = null, ResultSet $selectResultPrototype = null) {
        return parent::__construct('ng_tag', $adapter, $databaseSchema, $selectResultPrototype);
    }

    private function cargarCampos($datos = array()) {
        $this->serie = $datos["serie"];
        $this->id_automotora = $datos["automotora"];
        $this->id_clase = $datos["clase"];
        $this->id_carga = $datos["id_carga"];
        $this->id_estado = $datos["id_estado"];
    }

    public function nuevoTag($data = array()) {

        self::cargarCampos($data);
        
        $array = array
            (
            'serie' => $this->serie, 
            'id_automotora' => $this->id_automotora,
            'id_clase' => $this->id_clase,
            'id_carga' => $this->id_carga,
            'id_estado' => $this->id_estado,
        );

        $this->insert($array);
    }

    public function getTag($serie)
    {
        return $this->select(array('serie' => $serie))->toArray();
    }

    public function getTagCarga($id_carga)
    {
        return $this->select(array('id_carga' => $id_carga))->toArray();
    }

    public function actualizaEstado($data = array()) {

        self::cargarCampos($data);
        $array = array
            (
            'serie' => $this->serie, 
            'id_estado' => $this->id_estado,
        );

        $this->update($array, array('serie' => $data->serie));
        return true;
    }

    public function borrar($serie) 
    {
        $this->delete(array('serie' => $serie));
        return true;
    }
}
