<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\Model\Entity\DinamicView;
use Sistema\Model\Entity\Crm\UsuarioPerfilTable;
use Sistema\Model\Entity\Crm\PerfilTable;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Util\Encr;

use Zend\Session\Container;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;


class LoginController extends AbstractActionController
{
    public $dbAdapter;
    
    public function indexAction()
    {           
       $datos=Array();
       $request = $this->getRequest();      
       if ($request->isPost()) {
            $datos=$request->getPost();
            $accion=APP::ACTION_SEND;            
        }else{
            $accion=APP::ACTION_ERROR;            
        }
        return $this->forward()->dispatch('Application\Controller\Login',array('action'=>$accion,'datos'=>$datos));  
    }

    public function homeAction()
    {
        $sid = new Container('base');
        if($sid->offsetExists('url_sesion')){
            $url = $sid->offsetGet('url_sesion');
            return $this->redirect()->toUrl($url);
        }        
        $sid->getManager()->getStorage()->clear();
        $this->layout('layout/login'); 
        return new ViewModel();
    }

    public function errorAction()
    {
        $status=APP::STATUS_NOK;
        $error=APP::ERROR_LOGIN(0);
        return new JsonModel(array("status"=>$status,"error"=>$error));
    }   

    public function sendAction()
    {   
        //Obtenemos datos post             
        $data = $this->params()->fromRoute('datos');
        //Validamos si existe usuario y contraseņa
        if($data['user_adm']!=null && $data['password_adm']!=null){
            //Conectamos a BBDD
            $dbAdapter=$this->getServiceLocator()->get('Zend\Db\Adapter');
            //Consultamos Usuario y Password en tabla USUARIOS
            $UsuView = new DinamicView(VMAPP::usuario,$dbAdapter);
            $usuario = $UsuView->getUsuario($data['user_adm'],$data['password_adm']);
            //Validamos existencia
            if(!empty($usuario)){ 

                    $AccView = new DinamicView(VMAPP::acceso,$dbAdapter);
                    $acceso = $AccView->getAcceso($usuario[0]['id']);

                    if(count($acceso)<1){ 
                        $status=APP::STATUS_NOK;
                        $error=APP::ERROR_LOGIN(5);
                        return new JsonModel(array("status"=>$status,"error"=>$error));
                    }

                    //Seteamos url 
                    $urlHome = $acceso[0]['ruta_modulo'];

                    //Consultamos menu para el usuario
                    $MenView = new DinamicView(VMAPP::menu,$dbAdapter);                    


                    //Iniciamos la sesion
                    $sid = new Container('base');
                    $sid->offsetSet('menu', $MenView->getMenu($acceso[0]['id_perfil_modulo']));
                    $sid->offsetSet('acceso', $acceso);
                    $sid->offsetSet('usuario', $usuario);
                    $sid->offsetSet('urlHome',$urlHome);
                    $sid->offsetSet('logged', 'y');

                    //Respondemos a la vista
                    $status=APP::STATUS_OK;                  
                    return new JsonModel(array("status"=>$status,"url"=>$urlHome));                    
                                                                                                                 
                    }else{
                        $status=APP::STATUS_NOK;
                        $error=APP::ERROR_LOGIN(1);
                        return new JsonModel(array("status"=>$status,"error"=>$error));
                    }                 
                }else{
                    $status=APP::STATUS_NOK;
                    $error=APP::ERROR_LOGIN(1);
                    return new JsonModel(array("status"=>$status,"error"=>$error));
                }                                                                                                                      
    } 

    private function retornaErrorLogin($code_error)
    {
            $status=APP::STATUS_NOK;
            $error=APP::ERROR_LOGIN($code_error);
            return new JsonModel(array("status"=>$status,"error"=>$error));
    }

    public function emailclaveAction()
    {
        $mail = $_POST['mail'];
        $this->dbAdapter=$this->getServiceLocator()->get('Zend\Db\Adapter');
        $usu = new UsuarioTable($this->dbAdapter);
        $existe = $usu->getDatosMail($mail);        
        if (count($existe)>0)
        {
            $str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
            $cad = "";
                for($i=0;$i<8;$i++) {
                    $cad .= substr($str,rand(0,62),1);
                }
             $token = md5($cad);
            //$nuevaclave = substr($nuevaclave,3,11);                
         
             $nuevotoken = array('token'=>$token,'mail'=>$mail);
             $tok = new TokenTable($this->dbAdapter);                         
             $tok->nuevoToken($nuevotoken);
             $localhost = "";             
             $url = $localhost."/crm/public/application/recuperar/index/".$token;                                          
             $htmlMarkup = " html de correo recuperar contraseņa"  ;                                  
             
          /*   $text = new MimePart($textContent);
             $text->type = "text/plain";*/

             $html = new MimePart($htmlMarkup);
             $html->type = "text/html";

            /* $image = new MimePart(fopen($pathToImage, 'r'));
             $image->type = "image/jpeg";*/

             $body = new MimeMessage();
             $body->setParts(array($html));
        
             $message = new Message();
             $message->addTo($mail)
             ->addFrom('noreply@crmadmision.cl', 'CRM-Admision')
             ->setSubject('Recuperar Contraseņa')
             ->setBody($body);

             $transport = new SendmailTransport();
             $transport->send($message);                             
             $descripcion = "Se ha enviado un correo a :  ".$mail;                          
             $result = new JsonModel(array('descripcion'=>$descripcion,'status'=>'ok'));                                
             return $result;  
        }else{
          $descripcion="Lo siento, el correo no existe en nuestros registros...";    
          $result = new JsonModel(array('descripcion'=>$descripcion));                                
          return $result;
        }    
}        
    public function salirAction()
    {
       //Instancia de sesion
       $sid = new Container('base');
       
       //destruimos todas las sessiones
       $sid->getManager()->getStorage()->clear();
       
       //eliminamos Cookies
        if(isset($_COOKIE['usuario']))
        {
            unset($_COOKIE['usuario']);
            //setcookie('usuario', '', time() - 3600); // empty value and old timestamp
        }
         if(isset($_COOKIE['password']))
        {
            unset($_COOKIE['password']);
            //setcookie('password', '', time() - 3600); // empty value and old timestamp
        }
       // var_dump($this->getRequest()->getUri());
        return $this->redirect()->toUrl(APP::URL_HOME);
        
       
    }                           
}
