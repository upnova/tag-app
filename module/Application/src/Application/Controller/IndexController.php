<?php

namespace Application\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Console\Request as ConsoleRequest;
use RuntimeException;

use Application\Constants\VMAPP;
use Application\Constants\APP;
use Application\Constants\RTN;
use Application\Util\Encr;

use Application\Delegate\IndexDelegate;


use Zend\Session\Container;


class IndexController extends AbstractActionController
{ 
    private $index;
    public function __construct() {
        $this->index=new IndexDelegate();
    }
    public function indexAction()
    {
       //Validamos sesion
       $sid = new Container('base');        
       if($sid->offsetGet('logged') != APP::LOGGED){        
           return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home'));
       }

       $request = $this->getRequest();  
       if ($request->isPost()) {
           $datos=$request->getPost();
           return $this->forward()->dispatch('Application\Controller\Index',array('action'=>'routing','datos'=>$datos));   
       }else{
           return $this->forward()->dispatch('Application\Controller\Login',array('action'=>'home')); 
       }
        
    }

    public function routingAction()
    {
        $datos = $this->params()->fromRoute('datos');
        $result = $this->index->getUrl($datos);
        $url = $result['url'];
        $url_completa = $result['url_completa'];
        if($url_completa){
            $this->index->setSesion('url_sesion', $url);
            return $this->redirect()->toUrl($url);
        }else{
            return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/'.$url);
        }
    }  

    public function nofoundAction()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }
    
    
}

