<?php
namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;
use Application\Constants\APP;
use Application\Constants\VMAPP;

class DatoPersonaHelper extends AbstractHelper
{


   public function __invoke($parameter)
    {
            $sid = new Container('base');
            $datos="";
            if($sid->offsetExists('acceso')){
            	
                $usuario=$sid->offsetGet('acceso');
                if($parameter=="nombre_apellido"){
                    $datos = $usuario[0]['nombre_completo'];
                }
                if($parameter=="user_name"){
                    $datos = $usuario[0]['nombre']; 
                }

                if($parameter=="foto_perfil"){
                    if(isset($usuario[0]['foto'])){
                        $datos = APP::URL_AVATAR.$usuario[0]['foto']; 
                    }else{
                        $datos = APP::URL_AVATAR_NOT_FOUND; 
                    }
                }               
                 
            }
            return $datos;
    }
}
?>
