<?php

namespace Application\View\Helper;
use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;

class MenuHelper extends AbstractHelper
{

public function __invoke()
    {
       
    $sid = new Container('base');

    //Link Dashboard
    $printMenu = '<li id="m-0"><a class="linkMenu" data="escritorio#m-0"><i class="fas fa-chart-line"></i><span class="title">  Escritorio</span><span class="arrow "></span></a></li>';


         if ($sid->offsetExists('menu')){
             $menu=$sid->offsetGet('menu');
             $agrupa = self::zf2Group($menu);
             //$pathM=$_SERVER['REDIRECT_BASE'].$sid->offsetGet('modulo_ruta').'/';
             
            
            for($i=0;$i<count($agrupa);$i++){
                $classMenu="";
                if(!isset($agrupa[$i]['submenu'][0]['nombre'])){
                    $classMenu="class='linkMenu' data='".$agrupa[$i]['menu']['ruta'].'#m-'.$agrupa[$i]['menu']['id']."'";
                }
                
                $printMenu = $printMenu.'<li id="m-'.$agrupa[$i]['menu']['id'].'"><a '.$classMenu.'><i class="'.$agrupa[$i]['menu']['icono'].'"></i>';
                $printMenu = $printMenu.'<span class="title">  '.$agrupa[$i]['menu']['nombre'].'</span>';
                if(isset($agrupa[$i]['submenu'][0]['nombre'])){
                    $printMenu = $printMenu.'<span class="arrow "></span>';
                }
                $printMenu = $printMenu.'</a>';
                if(isset($agrupa[$i]['submenu'][0]['nombre'])){
                     $printMenu = $printMenu.'<ul class="sub-menu">';
                     
                     for($i1=0;$i1<count($agrupa[$i]['submenu']);$i1++){
                       $printMenu = $printMenu.'<li id="s-'.$agrupa[$i]['submenu'][$i1]['id'].'">';
		       //$printMenu = $printMenu.'<a href="'.$pathM.$agrupa[$i]['submenu'][$i1]['ruta'].'">
                       $printMenu = $printMenu.'<a class="linkMenu" data="'.$agrupa[$i]['submenu'][$i1]['ruta'].'#s-'.$agrupa[$i]['submenu'][$i1]['id'].'#m-'.$agrupa[$i]['menu']['id'].'">    
				<i class="'.$agrupa[$i]['submenu'][$i1]['icono'].'"></i>
				'.$agrupa[$i]['submenu'][$i1]['nombre'].'</a>
				</li> '; 
                     }
                     $printMenu = $printMenu.'</ul>';
                }
                $printMenu = $printMenu."</li>";
             }
         }
         //
    return $printMenu;
  }
    
    private static function zf2Group($menu){
        
        $temp=Array();
        $j=0;$y=0;
        for($idx=0;$idx<count($menu);$idx++){
            if($idx==0){
               $id_menu = $menu[$idx]['id_menu'];
               $temp[$j]['menu']=array('id'=>$id_menu ,'nombre'=>$menu[$idx]['nombre_menu'],'icono'=>$menu[$idx]['icono_menu'],'ruta'=>$menu[$idx]['ruta_menu']);
               $y=0;
               if(isset($menu[$idx]['id_submenu'])){
                  $temp[$j]['submenu'][$y]=array('id'=>$menu[$idx]['id_submenu'],'nombre'=>$menu[$idx]['nombre_submenu'],'icono'=>$menu[$idx]['icono_submenu'],'ruta'=>$menu[$idx]['ruta_submenu']); 
                  $y++;
               }
               $j++;
            }else{
                
                if($id_menu==$menu[$idx]['id_menu']){
                    if(isset($menu[$idx]['id_submenu'])){
                        
                        $temp[$j-1]['submenu'][$y]=array('id'=>$menu[$idx]['id_submenu'],'nombre'=>$menu[$idx]['nombre_submenu'],'icono'=>$menu[$idx]['icono_submenu'],'ruta'=>$menu[$idx]['ruta_submenu']); 
                        $y++;
                    }
                }else{
                        $id_menu = $menu[$idx]['id_menu'];
                        $temp[$j]['menu']=array('id'=>$id_menu,'nombre'=>$menu[$idx]['nombre_menu'],'icono'=>$menu[$idx]['icono_menu'],'ruta'=>$menu[$idx]['ruta_menu']);
                        $y=0;
                        if(isset($menu[$idx]['id_submenu'])){
                            $temp[$j]['submenu'][$y]=array('id'=>$menu[$idx]['id_submenu'],'nombre'=>$menu[$idx]['nombre_submenu'],'icono'=>$menu[$idx]['icono_submenu'],'ruta'=>$menu[$idx]['ruta_submenu']); 
                        $y++;
                        }
                        $j++;
                }
            
            }
                
           }
            
             return $temp;
        }
        
       
        
    
}

