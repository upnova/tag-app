function validaRut(id_rut,id_send){
   $('[data-toggle="popover"]').popover({placement:'auto top'})
   $("input#"+id_rut).rut({validateOn: 'change'}).on('rutInvalido', function(){ 
     $('#'+id_send).attr("disabled","disabled"); 
     $("input#"+id_rut).attr('data-content','Rut Incorrecto').css("background","#FD8C92").popover('show');    
 });
   $("input#"+id_rut).rut({validateOn: 'blur'}).on('rutValido', function(){
    $('#'+id_send).removeAttr("disabled");
    $("input#"+id_rut).popover('destroy').css("background","#fff");   
});     
}
function getView(controller,action,div_response){
$.ajax(
   {
    url : 'admin/'+controller+'/'+action,
    type: "GET",
        //data : postData,  
        beforeSend: function(){
            Pace.stop(); Pace.start();
        },      
        success : function(response) {                
            $("#"+div_response).hide().html(response).slideDown(200);             
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            toastr.warning("No se ha encontrado información.")      
        }
    });
}